import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component {
    axiosCall = async (url, body) => {
        const response = await axios(url, body);

        return response.data;
    }

    onBtnGetAllOrder = () => {
        console.log("Get All Order");

        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders") // Thay = link API
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    onBtnCreateOrder = () => {
        console.log("Create Order");
        const body = {
            method: 'POST',
            body: JSON.stringify({
                _id: 2,
                orderCode: "GLMRN2022",
                pizzaSize: "L",
                pizzaType: "Seafood",
                voucher: "6373b9d6e0bae0ea8a36a042",
                drink: "Coke",
                status: "Open"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    onBtnGetOrderById = () => {
        console.log("Get Order By Id");
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders/63813275854a73f7ba85956d")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onBtnUpdateOrder = () => {
        console.log("Update Order");
        const orderId = "63813275854a73f7ba85956d";
        const body = {
            method: "PUT",
            body: JSON.stringify({
                _id: 133,
                orderCode: "GLMRN20221",
                pizzaSize: "M",
                pizzaType: "Seafood",
                voucher:"6373b9d6e0bae0ea8a36a042",
                drink:"Coke",
                status:"Open"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders/" + orderId, body)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onBtnCheckVoucher = () => {
        console.log("Check Voucher");
        const voucherId = "6373badcdcd3ebe07ce5fbae"
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + voucherId)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onBtnGetDrinkList = () => {
        console.log("Get Drink List");
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/drinks/")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    render() {
        return (
            <>
                <button onClick={this.onBtnGetAllOrder} className="btn btn-info m-1">Call api get all order</button>
                <button onClick={this.onBtnCreateOrder} className="btn btn-primary m-1">Call api create order</button>
                <button onClick={this.onBtnGetOrderById} className="btn btn-success m-1">Call api get order by id</button>
                <button onClick={this.onBtnUpdateOrder} className="btn btn-secondary m-1">Call api update order</button>
                <button onClick={this.onBtnCheckVoucher} className="btn btn-danger m-1">Call api check voucher by id</button>
                <button onClick={this.onBtnGetDrinkList} className="btn btn-success m-1">Call api get get drink list</button>
            </>
        )
    }
}

export default AxiosLibrary;