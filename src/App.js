import "bootstrap/dist/css/bootstrap.min.css"
import AxiosLibrary from "./components/Axios";
import Fetch from "./components/Fetch";


function App() {
  return (
    <div>
      <h3>Fetch</h3>
      <Fetch />
      <hr />
      <h3>Axios Library</h3>
      <AxiosLibrary />
    </div>
  );
}

export default App;
